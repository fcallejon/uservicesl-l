#!/usr/bin/env bash

# set -x

# echo "arg 1 $1"
# apt-get update
# apt-get install -y iputils-ping wget
BASE_DIR=/usr/share/nginx/html

# apply environment variables to default.conf
envsubst < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf
dpkg -l | grep nginx-extras
echo 
nginx -V
exec nginx -g "daemon off;"

