﻿namespace shipping.Controllers

open System
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Configuration
open MySql.Data.MySqlClient
open System.Data
open System.IO
open FSharp.Data
open FSharp.Data.HttpRequestHeaders
open System.Text

type Code(code:string, name:string) =
    member x.code = code
    member x.name = name

type City(uuid:Int64, name:string) =
    member x.uuid = uuid
    member x.name = name

type Ship(dist:float, initialCost: double) =
    member x.distance = dist
    member x.cost = initialCost

type Location (lat:double, lon:double) =
    member this.latitude = lat
    member this.longitude = lon
    member this.GetDistance(targetLatitude:double) (targetLongitude:double) =
        let earthRadius = 6371e3; // meters

        let degreesToRadians (d : double) =
            d * System.Math.PI / 180.0

        let theta = (this.longitude - targetLongitude) |> degreesToRadians
        let rawDistance = Math.Sin(this.latitude |> degreesToRadians) * Math.Sin(targetLatitude |> degreesToRadians) +
                            Math.Cos(this.latitude |> degreesToRadians) * Math.Cos(targetLatitude |> degreesToRadians) * Math.Cos(theta)
        Math.Round(earthRadius * Math.Acos(rawDistance) / 1000.0)

[<Route("")>]
[<ApiController>]
type ShippingController(configuration:IConfiguration) =
    inherit ControllerBase()
    let _configuration = configuration
            
    let getValueOrDefault value defaultValue =
        match value with
        | null -> defaultValue
        | "" -> defaultValue
        | " " -> defaultValue
        | value -> value

    let dbConnectionString =
        let value = getValueOrDefault (_configuration.GetValue<string> "DB_HOST") "mysql"
        let user = getValueOrDefault (_configuration.GetValue<string> "DB_USER") "root"
        let password = getValueOrDefault (_configuration.GetValue<string> "DB_PASSWORD") ""
        String.Format("Server={0};Database=cities;User={1};Password={2};Auto Enlist=false; Convert Zero Datetime=true;", value, user, password);
      
    let cartUrl =
        let value = getValueOrDefault (_configuration.GetValue<string> "CART_ENDPOINT") "cart"
        String.Format("http://{0}/shipping/", value)
    
    [<HttpGet>]
    [<Route("health")>]
    member this.Health() =
        this.Ok()

    [<HttpGet>]
    [<Route("count")>]
    member this.Count() =
        async {
            let gen = fun (r:IDataReader) -> r.GetInt64(0)

            use db = new MySqlConnection(dbConnectionString)
            do! db.OpenAsync() |> Async.AwaitTask |> Async.Ignore
            let command = db.CreateCommand()
            command.CommandText <- "select count(*) as count from cities"
            command.CommandType <- CommandType.Text
            let! result = command.ExecuteReaderAsync(CommandBehavior.CloseConnection) |> Async.AwaitTask
            let data = Seq.toList(seq{while result.Read() && not result.IsClosed do
                                        yield gen result})
            let count = List.head data
            return this.Ok count
        }

    [<HttpGet>]
    [<Route("codes")>]
    member this.Codes() =
        async {
            let gen = fun (r:IDataReader) -> new Code(r.GetString(0), r.GetString(1))

            use db = new MySqlConnection(dbConnectionString)
            do! db.OpenAsync() |> Async.AwaitTask |> Async.Ignore
            let command = db.CreateCommand()
            command.CommandText <- "select code, name from codes order by name asc"
            command.CommandType <- CommandType.Text
            let! result = command.ExecuteReaderAsync(CommandBehavior.CloseConnection) |> Async.AwaitTask
            
            let data = Seq.toList(seq{while result.Read() && not result.IsClosed do
                                        yield gen result})
            return this.Ok data
        }

    [<HttpGet>]
    [<Route("cities/{code}")>]
    member this.Cities (code:string) =
        async {
            let gen = fun (r:IDataReader) -> new City(r.GetInt64(0), r.GetString(1))

            use db = new MySqlConnection(dbConnectionString)
            do! db.OpenAsync() |> Async.AwaitTask |> Async.Ignore
            let command = db.CreateCommand()
            command.CommandText <- "select uuid, name from cities where country_code = @id"
            command.CommandType <- CommandType.Text
            let param = command.CreateParameter()
            param.ParameterName <- "id"
            param.Value <- code
            command.Parameters.Add param |> ignore
            let! result = command.ExecuteReaderAsync(CommandBehavior.CloseConnection) |> Async.AwaitTask
            let data = Seq.toList(seq{while result.Read() && not result.IsClosed do
                                        yield gen result})
            return this.Ok data
        }

    [<HttpGet>]
    [<Route("match/{code}/{text}")>]
    member this.Match (code:string) (text:string) =
        async {
            let gen = fun (r:IDataReader) -> new City(r.GetInt64(0), r.GetString(1))

            use db = new MySqlConnection(dbConnectionString)
            do! db.OpenAsync() |> Async.AwaitTask |> Async.Ignore
            let command = db.CreateCommand()
            command.CommandText <- "select uuid, name from cities where country_code = @id and city like @text order by name asc limit 10"
            command.CommandType <- CommandType.Text
            let param = command.CreateParameter()
            param.ParameterName <- "id"
            param.Value <- code
            command.Parameters.Add param |> ignore
            let param2 = command.CreateParameter()
            param2.ParameterName <- "text"
            param2.Value <- String.concat "" ["%"; text; "%"]
            command.Parameters.Add param2 |> ignore
            let! result = command.ExecuteReaderAsync(CommandBehavior.CloseConnection) |> Async.AwaitTask
            
            let data = Seq.toList(seq{while result.Read() && not result.IsClosed do
                                        yield gen result})
            return this.Ok data
        }

    [<HttpGet>]
    [<Route("calc/{uuid}")>]
    member this.Calc (uuid:Int32) =
        async {
            let gen = fun (r:IDataReader) -> new Location(r.GetDouble(0), r.GetDouble(1))

            use db = new MySqlConnection(dbConnectionString)
            do! db.OpenAsync() |> Async.AwaitTask |> Async.Ignore
            let command = db.CreateCommand()
            command.CommandText <- "select latitude, longitude from cities where uuid = @id"
            command.CommandType <- CommandType.Text
            let param = command.CreateParameter()
            param.ParameterName <- "id"
            param.Value <- uuid
            command.Parameters.Add param |> ignore
            let! result = command.ExecuteReaderAsync(CommandBehavior.CloseConnection) |> Async.AwaitTask

            match result.HasRows with
            | false -> return this.NotFound uuid :> ObjectResult
            | true ->
                result.Read() |> ignore
                let location = gen result
                let homeLon = _configuration.GetValue<double> "HOME_LON"
                let homeLat = _configuration.GetValue<double> "HOME_LAT"
                let distance = location.GetDistance homeLat homeLon
                let shippingCost = Math.Round(distance * 5.0) / 100.0
                return this.Ok (new Ship(distance, shippingCost)) :> ObjectResult
        }

    [<HttpPost>]
    [<Route("confirm/{userId}")>]
    member this.Confirm (userId:string) =
        let readAllBytes (s : Stream) = 
            let ms = new MemoryStream()
            s.CopyTo(ms)
            ms.ToArray()
        async {
            let requestBody = this.Request.Body |> readAllBytes |> Encoding.UTF8.GetString
            let url = String.concat "" [cartUrl; userId]
            Console.WriteLine("About to send ... ")
            Console.WriteLine(requestBody)
            Console.WriteLine(String.concat "" ["To: "; url])
            let! response = Http.AsyncRequest(url, headers = [ ContentType HttpContentTypes.Json ], httpMethod = "POST", body = TextRequest requestBody)
            Console.WriteLine(String.Format("Got {0} ", response.StatusCode))
            match response.StatusCode with
            | 200 ->
                let responseBody = match response.Body with
                                    | Text body -> body
                                    | Binary buf -> buf |> Encoding.UTF8.GetString
                Console.WriteLine(responseBody)
                return this.Ok responseBody :> ObjectResult
            | _ -> return this.NotFound userId :> ObjectResult
            
        }
