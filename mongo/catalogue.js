//
// Products
//
db = db.getSiblingDB('catalogue');
db.products.insertMany([
    {sku: 'LPOP', name: 'Lollipops', description: 'a large, flat, rounded boiled sweet on the end of a stick.', price: 2001, instock: 2, categories: ['Type 1']},
    {sku: 'JLBN', name: 'Jelly Beans', description: 'a bean-shaped sweet with a jelly-like centre and a firm sugar coating.', price: 200, instock: 0, categories: ['Type 1']},
    {sku: 'M&M', name: 'M&M\'s Candy Packs', description: 'Classic milk chocolate ellipsoids covered in a thin candy shell of assorted colors!', price: 1200, instock: 12, categories: ['Type 2']},
    {sku: 'CGBR', name: 'Cherry Gummy Bears', description: 'If you’re obsessed with the idea of a yummy, red bear snack, you may have once or twice considered the possibility of nibbling on a red panda.', price: 5000, instock: 10, categories: ['Type 2']},
    {sku: 'GUMBALLS', name: 'Dubble Bubble Gumballs', description: 'GUMBALLS! Keep your machine filled with yummy, fresh gumballs in an assortment of 8 fabulous flavors:', price: 700, instock: 1, categories: ['Type 2']},
    {sku: 'MRSHMLW', name: 'Mini Marshmallows', description: 'Mini Marshmallows', price: 1400, instock: 1, categories: ['Type 2']},
    {sku: 'NERDS', name: 'Nerds Squishy', description: 'Whether you’re settling into bed to read a classic novel (you nerd!), working on calculus homework (how nerdy) or just taking a nap (we’ll consider this a nerdy action, too, for continuity’s sake), there’s no better place to rest your head than atop a Rainbow Nerds Candy Pillow!', price: 300, instock: 12, categories: ['Type 2']},
    {sku: 'SFCTR', name: 'Sugar Free Caramel Toffee Rolls', description: 'Creamy, chewy sugar free caramel toffee is wrapped in elegant metallic foils. Although they’re perfect for setting out in candy dishes to share with loved ones, we also have it on good authority that these toffee rolls make great gifts and bribes.', price: 700, instock: 5, categories: ['Type 2']},
    {sku: 'LTRUF', name: 'Chocolate Lindor Truffles', description: 'When you break LINDOR\'s delicate chocolate shell, the irresistibly smooth filling starts to melt, gently caressing all your senses and taking you to a place where chocolate dreams come true. Indulge your chocolate desires and experience the of enchantment of these decadent dark chocolate truffles again and again.', price: 42, instock: 48, categories: ['Type 2']},
    {sku: 'KK45', name: 'Kit Kat 4.5', description: 'Twelve jumbo-sized wafers, coated with jumbo-sized milk chocolate (which translates to jumbo-sized deliciousness in each jumbo-sized bite!)', price: 50, instock: 1000, categories: ['Type 2', 'Type 1']},
    {sku: 'PRCP', name: 'Pop Rocks Candy Packs', description: 'The original Pop Rocks candy invented in the 1970\'s featured a fabulous cherry flavor...and now it\'s back again!', price: 1000, instock: 0, categories: ['Type 2']}
]);

// full text index for searching
db.products.createIndex({
    name: "text",
    description: "text"
});

// unique index for product sku
db.products.createIndex(
    { sku: 1 },
    { unique: true }
);

