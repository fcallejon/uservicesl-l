# Sample Microservice Application

## Candy Shop is a sample microservice application to play with

### Run it

#### To run in powershell

```powershell
docker-compose -f "docker-compose.yaml" up -d --build
Start-Process -FilePath "http://localhost:8080"
```

#### To run loading

```powershell
cd loading
docker build -t candyshop/ccs-loading:latest .
docker run -e 'HOST=http://localhost:8080' -e 'NUM_CLIENTS=10' -d --rm --name="ccs-loading" candyshop/ccs-loading
```

Or:

```powershell
cd loading
pip install -r requirements.txt
locust -f candy-shop.py --host "http://localhost:8080" --no-web -c 1 -r 1
```

#### Tear down in powershell

```powershell
docker-compose -f "docker-compose.yaml" down
docker system prune
```

### Check logs

```powershell
docker logs -f uservices_catalogue_1
```

### Presentation

Open [here](https://slides.com/fernandocallejon-1/from-spaghetti-to-raviolis/fullscreen)